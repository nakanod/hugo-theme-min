+++
date = "{{ .Date }}"
#updated = "{{ .Date }}"
draft = true
markup = "md"
title = "{{ replace .TranslationBaseName "-" " " | title }}"
slug = "{{ .TranslationBaseName }}"
toc = false
tags = ["misc"]
+++
